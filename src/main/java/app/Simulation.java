package app;

import app.items.Item;
import app.vendors.Drill;
import app.vendors.OnlineShop;
import app.vendors.Tadek;
import app.vendors.Vendor;
import app.companies.Company;

import java.util.Map;

public class Simulation {

    public static void main(String[] args) {

        Company company = new Company("CompAmp");

        Vendor vendor = new OnlineShop();
        Item tent = new Item(1, 500.0d, "Tent");
        Item rope = new Item(2, 700.0d, "Rope");
        Item shoes = new Item(3, 320.0d, "Shoes");

        vendor.addToCart(tent);
        vendor.addToCart(rope);
        vendor.addToCart(shoes);

        vendor.sell(company);


        vendor = new Drill();
        Item nails = new Item(1, 2.0d, "Nails");
        Item screwdriver = new Item(2, 700.0d, "Screwdriver");
        vendor.addToCart(nails);
        vendor.addToCart(screwdriver);

        vendor.sell(company);


        vendor = new Tadek();
        Item beef = new Item(1, 10.0d, "Beef");
        Item fish = new Item(2, 25.0d, "Fish");
        Item shrimp = new Item(3, 30.0d, "Shrimps");
        vendor.addToCart(beef);
        vendor.addToCart(fish);
        vendor.addToCart(shrimp);

        vendor.sell(company);

        showItems(company);
    }

    public static void showItems(Company company) {
        System.out.println("Items bought by " + company.getName() + ":");
        for (Map.Entry<String, Double> items : company.getBoughtItems().entrySet()) {
            System.out.println("\t" + items.getKey() + " - " + items.getValue() + " zł");
        }

        System.out.println("Total price: \n\t" + company.totalPrice() + " zł");
    }
}
