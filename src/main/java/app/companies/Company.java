package app.companies;

import java.util.HashMap;
import java.util.Map;

public class Company {

    private final String name;
    private Map<String, Double> boughtItems;

    public Company(String name) {
        this.name = name;
        this.boughtItems = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public Map<String, Double> getBoughtItems() {
        return this.boughtItems;
    }

    public double totalPrice() {
        return boughtItems.
                values().
                stream().
                mapToDouble(Double::doubleValue).
                sum();
    }
}
