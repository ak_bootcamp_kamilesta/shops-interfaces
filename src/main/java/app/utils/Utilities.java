package app.utils;

import app.items.Item;
import app.vendors.Vendor;
import java.util.HashMap;
import java.util.Map;

public class Utilities {

    public static Map<String, Double> rearrangeItemsMap(Vendor vendor) {
        Map<String, Double> map = new HashMap<>();
        for (Map.Entry<Integer, Item> item : vendor.getItems().entrySet()) {
            map.put(item.getValue().getName(), item.getValue().getPrice());
        }
        return map;
    }
}
