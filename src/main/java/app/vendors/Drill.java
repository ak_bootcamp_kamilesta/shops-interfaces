package app.vendors;

import app.companies.Company;
import app.items.Item;
import app.utils.Utilities;
import java.util.HashMap;
import java.util.Map;

public class Drill extends Vendor implements GoToShop {

    private Map<Integer, Item> itemMap;

    public Drill() {
        this.itemMap = new HashMap<>();
    }

    @Override
    public Map<Integer, Item> getItems() {
        return new HashMap<>(this.itemMap);
    }

    @Override
    public void addToCart(Item item) {
        itemMap.put(item.getId(), item);
    }

    @Override
    public void sell(Company company) {
        System.out.println("Steps taken to buy in " + getClass().getSimpleName() + " shop:");
        goToShopMethod();
        Map<String, Double> rearrangedMap = Utilities.rearrangeItemsMap(this);
        company.getBoughtItems().putAll(rearrangedMap);
    }

    @Override
    public void goToShopMethod() {
        System.out.println("\tSomeone went to shop.");
    }



}
