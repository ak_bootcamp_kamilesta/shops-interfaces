package app.vendors;

import app.items.Item;
import app.utils.Utilities;
import app.companies.Company;
import java.util.HashMap;
import java.util.Map;

public class OnlineShop extends Vendor
        implements OnlineOrder,
        ReceiveInvoice,
        ReceiveOrderFromCourier {

    private Map<Integer, Item> itemMap;

    public OnlineShop() {
        this.itemMap = new HashMap<>();
    }

    @Override
    public Map<Integer, Item> getItems() {
        return new HashMap<>(this.itemMap);
    }

    @Override
    public void addToCart(Item item) {
        itemMap.put(item.getId(), item);
    }

    @Override
    public void sell(Company company) {
        System.out.println("Steps taken to buy on " + getClass().getSimpleName() + ":");
        onlineOrderMethod();
        receiveOrderFromCourierMethod();
        receiveInvoiceMethod();
        Map<String, Double> rearrangedMap = Utilities.rearrangeItemsMap(this);
        company.getBoughtItems().putAll(rearrangedMap);
    }

    @Override
    public void onlineOrderMethod() {
        System.out.println("\tItems was ordered online.");
    }

    @Override
    public void receiveInvoiceMethod() {
        System.out.println("\tInvoice was received from customer.");
    }

    @Override
    public void receiveOrderFromCourierMethod() {
        System.out.println("\tOrder was received from courier.");
    }
}
