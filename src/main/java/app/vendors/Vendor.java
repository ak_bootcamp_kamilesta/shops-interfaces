package app.vendors;

import app.items.Item;
import app.companies.Company;
import java.util.Map;

public abstract class Vendor {

    public abstract void addToCart(Item item);

    public abstract void sell(Company company);

    public abstract Map<Integer, Item> getItems();
}
