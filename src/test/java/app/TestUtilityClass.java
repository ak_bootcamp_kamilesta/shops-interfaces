package app;

import app.items.Item;

public class TestUtilityClass {

    public static final double PRICE_1 = 1;
    public static final double PRICE_2 = 2;
    public static final double PRICE_3 = 3;

    public static final double TOTAL_PRICE = PRICE_1 + PRICE_2 + PRICE_3;

    public static final String TEST_ITEM_NAME_1 = "TEST_ITEM_NAME_1";
    public static final String TEST_ITEM_NAME_2 = "TEST_ITEM_NAME_2";
    public static final String TEST_ITEM_NAME_3 = "TEST_ITEM_NAME_3";

    public static final Item ITEM_1 = new Item(1, PRICE_1, TEST_ITEM_NAME_1);
    public static final Item ITEM_2 = new Item(2, PRICE_2, TEST_ITEM_NAME_2);
    public static final Item ITEM_3 = new Item(3, PRICE_3, TEST_ITEM_NAME_3);
}
