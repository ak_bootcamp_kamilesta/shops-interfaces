package app.companies;

import app.TestUtilityClass;
import app.utils.Utilities;
import app.vendors.OnlineShop;
import app.vendors.Vendor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

@RunWith(JUnit4.class)
public class CompanyTest {

    private Vendor vendor;
    private Company company;

    @Before
    public void init() {
        this.vendor = new OnlineShop();
        this.company = new Company("TestCompany");

        vendor.addToCart(TestUtilityClass.ITEM_1);
        vendor.addToCart(TestUtilityClass.ITEM_2);
        vendor.addToCart(TestUtilityClass.ITEM_3);
    }

    @Test
    public void sumUpItemsPrices_shouldSumCorrectly() {
        Map<String, Double> testMap = Utilities.rearrangeItemsMap(vendor);
        company.getBoughtItems().putAll(testMap);
        assertThat(company.totalPrice()).isEqualTo(TestUtilityClass.TOTAL_PRICE );
    }
}

