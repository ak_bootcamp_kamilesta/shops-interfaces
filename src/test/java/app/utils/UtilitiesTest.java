package app.utils;

import app.TestUtilityClass;
import app.vendors.OnlineShop;
import app.vendors.Vendor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.*;

import java.util.Map;

@RunWith(JUnit4.class)
public class UtilitiesTest {

    private Vendor vendor;

    @Before
    public void init() {
        this.vendor = new OnlineShop();

        vendor.addToCart(TestUtilityClass.ITEM_1);
        vendor.addToCart(TestUtilityClass.ITEM_2);
        vendor.addToCart(TestUtilityClass.ITEM_3);
    }

    @Test
    public void rearrangeMap_shouldRearrange() {
        Map<String, Double> testMap = Utilities.rearrangeItemsMap(vendor);
        assertThat(testMap).contains(
                entry(TestUtilityClass.TEST_ITEM_NAME_1, TestUtilityClass.PRICE_1),
                entry(TestUtilityClass.TEST_ITEM_NAME_2, TestUtilityClass.PRICE_2),
                entry(TestUtilityClass.TEST_ITEM_NAME_3, TestUtilityClass.PRICE_3));
    }
}
