package app.vendors;

import app.TestUtilityClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;


@RunWith(JUnit4.class)
public class DrillTest {

    private Vendor drillShop;

    @Before
    public void init(){
        this.drillShop = new Drill();

        drillShop.addToCart(TestUtilityClass.ITEM_1);
        drillShop.addToCart(TestUtilityClass.ITEM_2);
        drillShop.addToCart(TestUtilityClass.ITEM_3);
    }

    @Test
    public void addItem_shouldAdd(){
        assertThat(drillShop.getItems()).contains(
                entry(TestUtilityClass.ITEM_1.getId(), TestUtilityClass.ITEM_1),
                entry(TestUtilityClass.ITEM_2.getId(), TestUtilityClass.ITEM_2),
                entry(TestUtilityClass.ITEM_3.getId(), TestUtilityClass.ITEM_3));
    }
}
