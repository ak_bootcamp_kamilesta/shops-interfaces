package app.vendors;

import app.TestUtilityClass;
import app.items.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.assertj.core.api.Assertions.*;


@RunWith(JUnit4.class)
public class OnlineShopTest {

    private Vendor onlineShop;

    @Before
    public void init(){
        this.onlineShop = new OnlineShop();

        onlineShop.addToCart(TestUtilityClass.ITEM_1);
        onlineShop.addToCart(TestUtilityClass.ITEM_2);
        onlineShop.addToCart(TestUtilityClass.ITEM_3);
    }

    @Test
    public void addItem_shouldAdd(){
        assertThat(onlineShop.getItems()).contains(
                entry(TestUtilityClass.ITEM_1.getId(), TestUtilityClass.ITEM_1),
                entry(TestUtilityClass.ITEM_2.getId(), TestUtilityClass.ITEM_2),
                entry(TestUtilityClass.ITEM_3.getId(), TestUtilityClass.ITEM_3));
    }
}
