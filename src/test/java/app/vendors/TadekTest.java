package app.vendors;

import app.TestUtilityClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;


@RunWith(JUnit4.class)
public class TadekTest {

    private Vendor tadekShop;

    @Before
    public void init(){
        this.tadekShop = new Tadek();

        tadekShop.addToCart(TestUtilityClass.ITEM_1);
        tadekShop.addToCart(TestUtilityClass.ITEM_2);
        tadekShop.addToCart(TestUtilityClass.ITEM_3);
    }

    @Test
    public void addItem_shouldAdd(){
        assertThat(tadekShop.getItems()).contains(
                entry(TestUtilityClass.ITEM_1.getId(), TestUtilityClass.ITEM_1),
                entry(TestUtilityClass.ITEM_2.getId(), TestUtilityClass.ITEM_2),
                entry(TestUtilityClass.ITEM_3.getId(), TestUtilityClass.ITEM_3));
    }
}
